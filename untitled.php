<?php
// Add scripts and stylesheets
add_action('wp_enqueue_scripts', 'custom_scripts');
function custom_scripts()
{
    wp_enqueue_style('bootstrap', get_template_directory_uri() .
        '/css/bootstrap.min.css', array(), '3.3.6');
    wp_enqueue_style('blog', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_script('bootstrap', get_template_directory_uri() .
        '/js/bootstrap.min.js', array('jquery'), '3.3.6', true);
}

add_action('after_setup_theme', 'custom_theme_setup');
function custom_theme_setup()
{

    add_theme_support('title-tag'); // WordPress Titles

    add_theme_support('post-thumbnails'); // images

    add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );

    add_theme_support( 'custom-logo' );//logo

    add_theme_support('post-formats', array('aside')); // textes
    
    add_theme_support( 'post-thumbnails', array( 'post' ) ); // Ajouter les images à la une sur les articles uniquement


    register_nav_menus(array( // déclaration d'un menu
        'primary' => 'Menu principal',
    ));
}


function bpc_comment_count() { 

    $comments_count = wp_count_comments();
    $message =  'There are <strong>'.  $comments_count->approved . '</strong> commentaires postés par nos visiteurs.';
    
    return $message; 
    
    } 
    
    add_shortcode('bpc_total_comments','wpb_comment_count'); 
    add_filter('widget_text','do_shortcode');


    if(function_exists('acf_add_options_page'))
    {
    acf_add_options_page();
    }


    ///=============================================================
    //  On déclare notre contenu personnalisé 
    function register_movie_post_type() {
        register_post_type( 'movie', array(
            'labels' => array(
                'name' => __( 'Movies' ),
                'singular_name' => __( 'Movie' ),
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
            ) ) );
    }