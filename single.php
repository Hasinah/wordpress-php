<?php get_header();?>
<!--==============================================sidebar=======================================================-->



<!--========================================================header===================================================================-->


<!--========================================================bolg===================================================================-->
<div class="content">
    <div class="container">

        <?php if (have_posts()):while (have_posts()): the_post();?>
            <?php get_template_part('content-single');?>
         <?php endwhile; endif;?>

  </div><!-- .col-xs-6 -->
</div><!-- .blog-grid -->
 


<div class="pagination">
   
     
    <a class="next page-numbers" href="<?php the_permalink() ?>"><i class="rsicon rsicon-chevron_left"></i></a>
     <?php next_posts_link('Older')?>
    <?php previous_posts_link('Newer')?> 
   
    <a class="page-numbers" href="<?php the_permalink() ?>">22</a>
    <a class="next page-numbers" href="<?php the_permalink() ?>"><i class="rsicon rsicon-chevron_right"></i></a>
</div><!-- .pagination -->
</div><!-- .blog -->
<!-- END: PAGE CONTENT -->
<?php get_sidebar();?>

</div><!-- .container -->
</div><!-- .content -->


<?php get_footer();?>


</div><!-- .wrapper -->
<!--========================================================bolg===================================================================-->
<a class="btn-scroll-top" href="<?php the_permalink() ?>#"><i class="rsicon rsicon-arrow-up"></i></a>
<div id="overlay"></div>
<div id="preloader">
  <div class="preload-icon"><span></span><span></span></div>
  <div class="preload-text">Loading ...</div>
</div>

<!-- Scripts -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_directory');?>/js/site.js"></script>
<?php wp_footer()?>
</body>
</html>