<!-- START: PAGE CONTENT -->

        <div class="grid-item">
            <article class="post-box animate-up">
                <div class="post-media">
                    <div class="post-image">
                        <a href="single.html"><img src="<?php the_permalink() ?>" alt=""></a>
                        <?php if ( has_post_thumbnail() )
                        the_post_thumbnail();?>
                    </div>
                </div>

                <div class="post-data">
                    <time class="post-datetime">
                        <span class="day"><?php echo get_the_date('d') ?></span>
                        <span class="month"><?php echo get_the_date('m') ?></span>
                    </time>

                    <div class="post-tag">
                        <a href="category.html">#Photo</a>
                        <a href="category.html">#Architect</a>
                    </div>

                    <h3 class="post-title">
                       <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                    </h3>

                    <div class="post-info">
                        <a href="category.html"><i class="rsicon rsicon-user"></i><a href="<?php the_permalink() ?>"><?php the_author() ?></a></a>
                        <a href="category.html"><i class="rsicon rsicon-comments"></i><a href="<?php the_permalink() ?>"><?php echo get_comments_number() ?></a>
                    </div>
                </div>
            </article>
        </div>
    