<?php
// add scripts and stylesheets
add_action( 'wp_enqueue_scripts','custom_scripts' );
function custom_scripts()
{

//wp_enqueue_style('bootstrap',"https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css",array(),'3.3.6');
	wp_enqueue_style('map-icons', get_bloginfo('template_directory') . '/fonts/map-icons/css/map-icons.min.css' );
	wp_enqueue_style('theme-color', get_bloginfo('template_directory') . '/colors/theme-color.css' );
	wp_enqueue_style('bxslider', get_bloginfo('template_directory') . '/js/plugins/jquery.bxslider/jquery.bxslider.css' );
	wp_enqueue_style('customscrol', get_bloginfo('template_directory') . '/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css' );
	wp_enqueue_style('mediaelement', get_bloginfo('template_directory') . '/js/plugins/jquery.mediaelement/mediaelementplayer.min.css' );
	wp_enqueue_style('mediaelement', get_bloginfo('template_directory') . '/js/plugins/jquery.mediaelement/mediaelementplayer.min.css' );
	wp_enqueue_style('fancybox', get_bloginfo('template_directory') . '/js/plugins/jquery.fancybox/jquery.fancybox.css' );
	wp_enqueue_style('fonts', get_bloginfo('template_directory') . '/fonts/icomoon/style.css' );
	wp_enqueue_style('owlcarousel-carousel', get_bloginfo('template_directory') . '/js/plugins/jquery.owlcarousel/owl.carousel.css' );
	wp_enqueue_style('owlcarousel-theme', get_bloginfo('template_directory') . '/js/plugins/jquery.owlcarousel/owl.theme.css' );
	wp_enqueue_style('optionpanel-panel', get_bloginfo('template_directory') . '/js/plugins/jquery.optionpanel/option-panel.css' );
	wp_enqueue_style('style-css', get_bloginfo('template_directory') . '/style.css' );
	wp_enqueue_script('javascript', get_bloginfo('template_directory') . '/js/libs/modernizr.js' );
	wp_enqueue_style('Fredoka',"https://fonts.googleapis.com/css?family=Fredoka+One");
	wp_enqueue_style('Open+Sans',"https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic");
	wp_enqueue_script( 'javascript', get_template_directory_uri() .
		'/js/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );


	/*==== les 2 lignes sont pour js comme dans notre them pas js donc on met en commentair si non on fait pareil en en haut ds le chemin on met le liens====*/

//wp_enqueue_style('boostrap', get_template_directory_uri()) . 
//'/js/boostrap.min.js' array( 'jquery'),'3.3.6', true);


}
// le titre et l'image 
add_action('after_setup_theme','custom_theme_setup');
function custom_theme_setup()
{
	add_theme_support('title-tag'); // WordPress Titles
	add_theme_support('post-thumbnails'); // images
	add_theme_support( 'custom-logo' );//logo
	add_theme_support('post-formats', array('aside')); // textes
	register_nav_menus(array('primary'=>'Menu principal'));//// !!!!attention pa d'espace entre caout
	add_theme_support( 'post-thumbnails', array( 'post' ) ); // Ajouter les images à la une sur les articles uniquement
}
	Function bpc_comment_count() { 

    $comments_count = wp_count_comments();
    $message =  'There are <strong>'.  $comments_count->approved . '</strong> commentaires postés par nos visiteurs.';
    
    return $message; 
    
    } 
    
    add_shortcode('bpc_total_comments','wpb_comment_count'); 
    add_filter('widget_text','do_shortcode');

    function register_article_post_type(){
	register_post_type('article',array('labels' => array('name' =>__('Articles'),'singular_name'=> __('Articles'),
),
	'public'=> true,
	'has_archive'=> true,
	'supports'=> array(
		'title',
		'editor',
		'thumbnail', 
	)
	));

}


    if(function_exists('acf_add_options_page'))
    {
    acf_add_options_page();
    }
	






