        <div class="content">
            <div class="container">

            <!-- START: PAGE CONTENT -->            
            <div class="row animate-up">
                <div class="col-sm-8">
                    <main class="post-single">
                        <article class="post-content section-box">
                            <div class="post-media">
                                <a href="<?php the_permalink() ?>"><img src="<?php the_permalink() ?>" alt=""></a>
                                <?php if ( has_post_thumbnail() )
                                the_post_thumbnail();?>                                
                            </div><!-- .post-media -->
                            
                            <div class="post-inner">                                                            
                                <header class="post-header">
                                    <div class="post-data">
                                        <div class="post-tag">
                                            <a href="<?php the_permalink() ?>">#Photo</a>
                                            <a href="<?php the_permalink() ?>">#Architect</a>
                                        </div>

                                        <div class="post-title-wrap">
                                            <h1 class="post-title"><?php the_title() ?></h1>
                                            <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
                                                <span class="day"><?php echo get_the_date('d') ?></span>
                                                <span class="month"><?php echo get_the_date('m') ?></span>
                                            </time>
                                        </div>

                                        <div class="post-info">
                                            <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-user"></i><a href="<?php the_permalink() ?>"><?php the_author() ?></a>
                                            <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><a href="<?php the_permalink() ?>"><?php echo get_comments_number() ?></a></a>
                                          
                                   
                                        </div>
                                    </div>
                                </header>

                                <div class="post-editor clearfix">
                                    <p> <?php the_excerpt();?>
                                    </p>
                                    <blockquote>
                                        <p><?php the_excerpt() ?><br>
                                            <cite><a href="<?php the_permalink() ?>"><?php the_author() ?></a> - <?php the_title() ?></cite>
                                        </p>
                                    </blockquote>

                                    <figure id="attachment_905" style="width: 509px;" class="wp-caption alignright">
                                        <img class="size-full wp-image-905 " title="Image Alignment 509x319"
                                             alt="Image Alignment 509x319" src="img/uploads/rs-thumb-509x319.jpg" width="509"
                                             height="319">
            
                                        <figcaption class="wp-caption-text">Feels good to be right all the time.</figcaption>
                                    </figure>

                                    <p><?php the_excerpt() ?></p>

                                    <h2><?php the_title() ?></h2>

                                   

                                    <p><?php the_excerpt() ?></p>
                                </div>

                                <footer class="post-footer">
                                    <div class="post-share">
                                        <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-503b5cbf65c3f4d8" async="async"></script>
                                        <div class="addthis_sharing_toolbox"></div>
                                    </div>
                                </footer>
                            </div><!-- .post-inner -->
                        </article><!-- .post-content -->

                        <nav class="post-pagination section-box">
                            <div class="post-next">
                                <div class="post-tag">Previous Article <a href="<?php the_permalink() ?>">#Tagapple</a></div>
                                <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php previous_post_link(); ?></a></h3>

                                <div class="post-info">
                                    <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-user"></i><?php the_author() ?></a>
                                    <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a>
                                </div>
                            </div>
                            <div class="post-prev">
                                <div class="post-tag">Next Article <a href="<?php the_permalink() ?>">#Photography</a></div>
                                <h3 class="post-title"><a href="<?php the_permalink() ?>"><?php previous_post_link(); ?></a></h3>

                                <div class="post-info">
                                    <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-user"></i><?php the_author() ?></a>
                                    <a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a>
                                </div>
                            </div>
                        </nav><!-- .post-pagination -->

                        <div class="post-comments">
                            <h2 class="section-title"><a href="<?php the_permalink() ?>"><?php echo get_comments_number() ?></a></h2>
                            
                            <div class="section-box">
                                <ol class="comment-list">
                                    <li class="comment">
                                        <article class="comment-body">
                                            <div class="comment-avatar">
                                                <img src="<?php 
   echo get_avatar( $id_or_email, $size = '96', $default = '<path_to_url>' ); 
   ?>" alt="avatar"/>
                                            </div>
                                            <div class="comment-content">
                                                <div class="comment-meta">
                                                    <span class="name"><?php the_author() ?></span>
                                                    <time class="date" datetime="2015-03-20T13:00:14+00:00">March 20, 2015 at 1:00 pm</time>
                                                    <a class="reply-link" href="<?php the_permalink() ?>#comment-reply">Reply</a>
                                                </div>
                                                <div class="comment-message">
                                                    <p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
                                                        assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
                                                </div>
                                            </div>
                                        </article>

                                        <ol>
                                            <li class="comment">
                                                <article class="comment-body">
                                                    <img class="comment-avatar" src="<?php if ( has_post_thumbnail() )
                                the_post_thumbnail();?>" alt="avatar"/>
                                                    <div class="comment-content">
                                                        <div class="comment-meta">
                                                            <span class="name">Jane Doe</span>
                                                            <time class="date" datetime="2015-03-20T13:00:14+00:00">March 20, 2015 at 1:00 pm</time>
                                                            <a class="reply-link" href="<?php the_permalink() ?>#comment-reply">Reply</a>
                                                        </div>
                                                        <div class="comment-message">
                                                            <p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
                                                                assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
                                                        </div>
                                                    </div>
                                                </article>

                                                <ol>
                                                    <li class="comment">
                                                        <article class="comment-body">
                                                            <img class="comment-avatar" src="<?php if ( has_post_thumbnail() )
                                the_post_thumbnail();?>" alt="avatar" />
                                                            <div class="comment-content">
                                                                <div class="comment-meta">
                                                                    <span class="name">Jane Doe</span>
                                                                    <time class="date" datetime="2015-03-20T13:00:14+00:00">March 20, 2015 at 1:00 pm</time>
                                                                    <a class="reply-link" href="<?php the_permalink() ?>#comment-reply">Reply</a>
                                                                </div>
                                                                <div class="comment-message">
                                                                    <p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
                                                                        assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </li><!-- .comment (level 3) -->
                                                </ol><!-- .comment-list (level 3) -->
                                            </li><!-- .comment (level 2) -->
                                        </ol><!-- .comment-list (level 2) -->
                                    </li><!-- .comment -->

                                    <li class="comment">
                                        <article class="comment-body">
                                            <img class="comment-avatar" src="img/rs-avatar-64x64.jpg" alt="avatar"/>
                                            <div class="comment-content">
                                                <div class="comment-meta">
                                                    <span class="name">Jane Doe</span>
                                                    <time class="date" datetime="2015-03-20T13:00:14+00:00">March 20, 2015 at 1:00 pm</time>
                                                    <a class="reply-link" href="<?php the_permalink() ?>#comment-reply">Reply</a>
                                                </div>
                                                <div class="comment-message">
                                                    <p>Lorem ipsum dolor sit aum nulla quis nesciunt ipsa aliquam aliquid eum, voluptatibus
                                                        assumenda minima vel. Eaque, velit architecto error ducimus aliquid.</p>
                                                </div>
                                            </div>
                                        </article>
                                    </li><!-- .comment -->
                                </ol><!-- .comment-list -->

                                <div id="comment-reply" class="comment-reply">
                                    <form>
                                        <div class="input-field">
                                            <input type="text" name="rs-comment-name"/>
                                            <span class="line"></span>
                                            <label>Name *</label>
                                        </div>

                                        <div class="input-field">
                                            <input type="email" name="rs-comment-email"/>
                                            <span class="line"></span>
                                            <label>Email *</label>
                                        </div>

                                        <div class="input-field">
                                            <input type="text" name="rs-comment-website"/>
                                            <span class="line"></span>
                                            <label>Website</label>
                                        </div>

                                        <div class="input-field">
                                            <textarea rows="4" name="rs-comment-message"></textarea>
                                            <span class="line"></span>
                                            <label>Type Comment Here *</label>
                                        </div>

                                        <div class="text-right">
                                            <span class="btn-outer btn-primary-outer ripple">
                                                <input class="btn btn-lg btn-primary" type="button" value="Leave Comment">
                                            </span>
                                        </div>
                                    </form>
                                </div><!-- .comment-reply -->
                            </div><!-- .section-box -->
                        </div><!-- .post-comments -->
                    </main>
                    <!-- .post-single -->
                </div>
                <div class="col-sm-4">
                    <div class="sidebar sidebar-default">
                        <div class="widget-area">
                            <aside class="widget widget-profile">
                                <div class="profile-photo">
                                    <img src="img/uploads/rs-photo-v2.jpg" alt="Robert Smith"/>
                                </div>
                                <div class="profile-info">
                                    <h2 class="profile-title">Robert Smith</h2>
                                    <h3 class="profile-position">Developer and businessman</h3>
                                </div>
                            </aside><!-- .widget-profile -->

                            <aside class="widget widget_search">
                                <h2 class="widget-title">Search</h2>
                                <form class="search-form">
                                    <label class="ripple">
                                        <span class="screen-reader-text">Search for:</span>
                                        <input class="search-field" type="search" placeholder="Search">
                                    </label>
                                    <input type="submit" class="search-submit" value="Search">
                                </form>
                            </aside><!-- .widget_search -->

                            <aside class="widget widget_contact">
                                <h2 class="widget-title">Contact Me</h2>
                                <form class="contactForm" action="https://rscard.px-lab.com/html/php/contact_form.php" method="post">
                                    <div class="input-field">
                                        <input class="contact-name" type="text" name="name"/>
                                        <span class="line"></span>
                                        <label>Name</label>
                                    </div>

                                    <div class="input-field">
                                        <input class="contact-email" type="email" name="email"/>
                                        <span class="line"></span>
                                        <label>Email</label>
                                    </div>

                                    <div class="input-field">
                                        <input class="contact-subject" type="text" name="subject"/>
                                        <span class="line"></span>
                                        <label>Subject</label>
                                    </div>

                                    <div class="input-field">
                                        <textarea class="contact-message" rows="4" name="message"></textarea>
                                        <span class="line"></span>
                                        <label>Message</label>
                                    </div>

                                    <span class="btn-outer btn-primary-outer ripple">
                                        <input class="contact-submit btn btn-lg btn-primary" type="submit" value="Send"/>
                                    </span>
                                    
                                    <div class="contact-response"></div>
                                </form>
                            </aside><!-- .widget_contact -->

                            <aside class="widget widget-popuplar-posts">
                                <h2 class="widget-title">Popular posts</h2>
                                <ul>
                                    <li>
                                        <div class="post-media"><a href="category.html"><img src="<?php the_permalink() ?>" alt=""></a>
                                <?php if ( has_post_thumbnail() )
                                the_post_thumbnail();?></a></div>
                                        <h3 class="post-title">
                       <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                    </h3>
                                        <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><a href="<?php the_permalink() ?>"><?php echo get_comments_number() ?></a></div>
                                    </li>
                                    
                                
                                </ul>
                            </aside><!-- .widget-popuplar-posts -->

                            <aside class="widget widget_tag_cloud">
                                <h2 class="widget-title">Tag Cloud</h2>
                                <div class="tagcloud">
                                    <a href="<?php the_permalink() ?>" title="1 topic">Business</a>
                                    <a href="<?php the_permalink() ?>" title="9 topics">City</a>
                                    <a href="<?php the_permalink() ?>" title="10 topics">Creative</a>
                                    <a href="<?php the_permalink() ?>" title="6 topics">Fashion</a>
                                    <a href="<?php the_permalink() ?>" title="2 topics">Music</a>
                                    <a href="<?php the_permalink() ?>" title="5 topics">News</a>
                                    <a href="<?php the_permalink() ?>" title="9 topics">Peoples</a>
                                </div>
                            </aside><!-- .widget_tag_cloud -->

                            <aside class="widget widget-recent-posts">
                                <h2 class="widget-title">Recent posts</h2>
                                <ul>
                                    <li>
                                        
                                        <h3 class="post-title"><a href="<?php the_permalink() ?>"><ul id="slider-id" class="slider-class">
    <?php
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 3, // Number of recent posts thumbnails to display
        'post_status' => 'publish' // Show only the published posts
    ));
    foreach($recent_posts as $post) : ?>
        <li>
            <div class="post-tag">
                                            <a href="<?php the_permalink() ?>">#Photo</a>
                                            <a href="<?php the_permalink() ?>">#Architect</a>
                                        </div>
            <a href="<?php echo get_permalink($post['ID']) ?>">


                <p class="slider-caption-class"><?php echo $post['post_title'] ?></p>

            </a>

            <div class="post-info"><a href="<?php the_permalink() ?>"><i class="rsicon rsicon-comments"></i><?php echo get_comments_number() ?></a></div>
        </li>
    <?php endforeach; wp_reset_query(); ?>
</ul></a></h3>
                                       
                                    </li>
                                    
                                </ul>
                            </aside><!-- .widget-recent-posts -->

                            <aside class="widget widget_categories">
                                <h2 class="widget-title">Categories</h2>
                                <ul>
                                    <li><a href="<?php the_permalink() ?>" title="Architecture Category Posts">Architecture</a> (9)</li>
                                    <li><a href="<?php the_permalink() ?>" title="Business Category Posts">Business</a> (16)</li>
                                    <li><a href="<?php the_permalink() ?>" title="Creative Category Posts">Creative</a> (18)</li>
                                    <li><a href="<?php the_permalink() ?>" title="Design Category Posts">Design</a> (10)</li>
                                    <li><a href="<?php the_permalink() ?>" title="Development Category Posts">Development</a> (14)</li>
                                    <li><a href="<?php the_permalink() ?>" title="Education Category Posts">Education</a> (9)</li>
                                </ul>
                            </aside><!-- .widget_categories -->
                        </div><!-- .widget-area -->
                    </div><!-- .sidebar -->
                </div><!-- .col-sm-4 -->
           
            </div><!-- .row -->         
            <!-- END: PAGE CONTENT -->
                
            </div><!-- .container -->
        </div><!-- .content -->